import { Bottleneck } from '../src'

describe('Bottleneck', () => {
  describe('run', () => {
    it('runs async tasks', async () => {
      const neck = new Bottleneck()
      const mock = jest.fn().mockResolvedValue(true)
      await neck.run(mock)
      expect(mock).toHaveBeenCalled()
    })
    it('runs async tasks concurrently', async () => {
      let count = 0
      const neck = new Bottleneck(4)
      const mock = jest.fn().mockImplementation(async () => {
        await sleep(200)
        count++
      })
      await Promise.all([
        neck.run(mock),
        neck.run(mock),
        neck.run(mock),
        neck.run(mock),
        (async () => {
          await sleep(100)
          expect(count).toEqual(0)
          expect(mock).toHaveBeenCalledTimes(4)
        })()
      ])
      expect(count).toEqual(4)
      expect(mock).toHaveBeenCalledTimes(4)
    })
    it('runs async tasks concurrently within a limit', async () => {
      let count = 0
      const neck = new Bottleneck(2)
      const mock = jest.fn().mockImplementation(async () => {
        await sleep(200)
        count++
      })
      await Promise.all([
        neck.run(mock),
        neck.run(mock),
        neck.run(mock),
        neck.run(mock),
        neck.run(mock),
        neck.run(mock),
        (async () => {
          await sleep(100)
          expect(count).toEqual(0)
          expect(mock).toHaveBeenCalledTimes(2)
          await sleep(200)
          expect(count).toEqual(2)
          expect(mock).toHaveBeenCalledTimes(4)
          await sleep(200)
          expect(count).toEqual(4)
          expect(mock).toHaveBeenCalledTimes(6)
        })()
      ])
      expect(count).toEqual(6)
      expect(mock).toHaveBeenCalledTimes(6)
    })
    it('forwards results of resolved tasks', async () => {
      const neck = new Bottleneck(1)
      const mock = jest.fn().mockResolvedValue(true)
      await expect(neck.run(mock)).resolves.toEqual(true)
    })
    it('forwards errors of rejected tasks', async () => {
      const neck = new Bottleneck(1)
      const mock = jest.fn().mockRejectedValue(new Error('oops'))
      await expect(neck.run(mock)).rejects.toThrow('oops')
    })
    it('keeps draining queue after rejections', async () => {
      const neck = new Bottleneck(4)
      const mockPass = jest.fn().mockResolvedValue(true)
      const mockFail = jest.fn().mockRejectedValue(new Error('oops'))
      await Promise.all([
        expect(neck.run(mockPass)).resolves.toEqual(true),
        expect(neck.run(mockFail)).rejects.toThrow('oops'),
        expect(neck.run(mockPass)).resolves.toEqual(true),
        expect(neck.run(mockFail)).rejects.toThrow('oops'),
        expect(neck.run(mockPass)).resolves.toEqual(true),
        expect(neck.run(mockFail)).rejects.toThrow('oops'),
        expect(neck.run(mockPass)).resolves.toEqual(true),
        expect(neck.run(mockFail)).rejects.toThrow('oops')
      ])
      expect(mockPass).toHaveBeenCalledTimes(4)
      expect(mockFail).toHaveBeenCalledTimes(4)
    })
  })
})

async function sleep (ms: number): Promise<void> {
  return await new Promise((resolve) => {
    setTimeout(() => resolve(), ms)
  })
}
