# @nfi/bottleneck
[![npm version][npm-image]][npm-url]
[![pipeline status][pipeline-image]][pipeline-url]
[![coverage status][coverage-image]][coverage-url]
[![standard-js][standard-image]][standard-url]

A semaphore for promises.

## Installation
```
npm install @nfi/bottleneck
```

## Documentation
API documentation is available [here](https://cptpackrat.gitlab.io/bottleneck).

[npm-image]: https://img.shields.io/npm/v/@nfi/bottleneck.svg
[npm-url]: https://www.npmjs.com/package/@nfi/bottleneck
[pipeline-image]: https://gitlab.com/cptpackrat/bottleneck/badges/master/pipeline.svg
[pipeline-url]: https://gitlab.com/cptpackrat/bottleneck/commits/master
[coverage-image]: https://gitlab.com/cptpackrat/bottleneck/badges/master/coverage.svg
[coverage-url]: https://gitlab.com/cptpackrat/bottleneck/commits/master
[standard-image]: https://img.shields.io/badge/code%20style-standard-brightgreen.svg
[standard-url]: http://standardjs.com/
