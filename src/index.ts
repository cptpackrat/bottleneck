
/** A semaphore for promises; maintains and drains a queue of async tasks,
  * ensuring no more than a specified maximum are ever running concurrently. */
export class Bottleneck {
  private readonly queue: Array<() => void> = []
  private readonly limit: number
  private active = 0

  /** @param limit Maximum concurrent tasks. */
  constructor (limit: number = 1) {
    this.limit = limit
  }

  /** Queue an async task to be run task as soon as the next free
    * slot is available; returns a promise that resolves to the
    * result of the async task.
    * @param task Task to run. */
  /* eslint-disable-next-line @typescript-eslint/no-invalid-void-type */
  async run<T = void> (cb: () => Promise<T>): Promise<T> {
    let _resolve: () => void
    const promise = new Promise<void>((resolve) => {
      _resolve = resolve
    })
    /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
    this.queue.push(_resolve!)
    this.shift()
    await promise
    try {
      return await cb()
    } finally {
      this.active--
      this.shift()
    }
  }

  /** Run next task if one exists and a free slot is available. */
  private shift (): void {
    if (this.active < this.limit && this.queue.length > 0) {
      const resolve = this.queue.shift()
      if (resolve !== undefined) {
        this.active++
        resolve()
      }
    }
  }
}
